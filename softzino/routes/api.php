<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'Api\UserAuthController@register')->name('register');
Route::post('/login', 'Api\UserAuthController@login')->name('login');

Route::middleware('auth:api')->group(function () {
    Route::get('user-list', 'Api\UserAuthController@user_list');
    Route::get('user/{id}', 'Api\UserAuthController@user_details');
});
