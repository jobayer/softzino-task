<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use App\Repositories\UserAuthRepository;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserAuthController extends Controller
{
    protected $user_auth;

    public function __construct(UserAuthRepository $user_auth)
    {
        $this->user_auth = $user_auth;
    }

    public function register(Request $request)
    {
        $data = $request->validate([
            'firstName' => 'required|max:255',
            'lastName' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);
        $data['password'] = bcrypt($request->password);
        $user = $this->user_auth->create($data);
        $token = $user->createToken('API Token')->accessToken;

        return response([ 'user' => $user, 'token' => $token, 'message' => 'Registration Successful']);
    }

    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($data)) {
            return response(['error' => true, 'error_message' => 'Incorrect credential. Please try again'], 200);
        }

        $token = auth()->user()->createToken('API Token')->accessToken;

        return response(['user' => auth()->user(), 'token' => $token, 'message' => 'Login Successful'], 201);
    }

    public function logout (Request $request) {
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }

    public function user_list()
    {
        $users = $this->user_auth->getAll();
        return response([ 'users' => UserResource::collection($users), 'message' => 'Successful'], 200);
    }

    public function user_details($id)
    {
        $user = $this->user_auth->getById($id);
//        dd($user->roles[0]->role->name);
        return response([ 'user' => $user, 'message' => 'Successful'], 200);
    }

}
