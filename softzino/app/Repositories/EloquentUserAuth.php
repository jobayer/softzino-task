<?php


namespace App\Repositories;
use App\Repositories\UserAuthRepository;
use App\User;

class EloquentUserAuth implements UserAuthRepository
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }
    public function getAll()
    {
        return $this->model->all();
    }
    public function getById($id)
    {
        return $this->model->findById($id);
    }
}
