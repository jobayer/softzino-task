<?php


namespace App\Repositories;


interface UserAuthRepository
{

    public function create(array $attributes);
    public function getAll();
    public function getById($id);
}
